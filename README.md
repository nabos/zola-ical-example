# Simple Example of a iCal support for the static site generator Zola

There is one event declared in `content/events` with all the needed extra front matter.  
The endpoint used to access the generated iCal is `<root_url>/calendar`
