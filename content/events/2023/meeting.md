+++
title = "Monthly Meeting"

# publish date
date = "2023-12-18"

[extra]
start_date = "2024-01-25"
start_time = "13h30"
end_date = "2024-01-25"
end_time = "16h00"
location = "Online at <url> or in meeting room A-242"
+++
